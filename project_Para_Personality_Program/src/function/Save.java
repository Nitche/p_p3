package function;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;


public class Save {


	private long startTime = 0;
	private long stopTime = 0;
	private long tempTime;		//今までの累計起動時間を保管する変数
	private String savePlacePath = "C:\\Users\\忠許\\Documents\\test.txt";	//セーブデータの保管場所のパス。後に入力に対応するようにする
	//private String[] launcherPath;
	private ArrayList<String> pathArray = new ArrayList<String>();
	//private int dataSize = 1;		//セーブデータのサイズ（行数）。デフォルトでは既に累計起動時間の項があるので１となっている。
	private File saveDataFile;
	private int check = 1;	//一行目の判定に使う
	
	public long totalTime = 0;
	
	
	
	//コンストラタ。起動時間の記録とデータの読み込みを行う
	public Save(){
	
		//System.out.println(System.getProperty("file.encoding"));
		startTime = System.currentTimeMillis();
		
		filePrepare();
		loadData();
	}
	
	
	//データファイルの書き込みや読み込みを行うための下準備
	public void filePrepare(){
			saveDataFile = new File(savePlacePath);
	}
	

	
	public void totalTimeCal(){
		stopTime = System.currentTimeMillis();
		totalTime = Math.round((stopTime - startTime + tempTime) / 1000);
	}
	
	
	//全データをセーブするメソッド
	public void saveDataAll(){
		
		totalTimeCal();
		
		try{
			if (checkBeforeWritefile(saveDataFile)){
		        PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(saveDataFile)));
		        
		        pw.println("time:" + totalTime);
		        
		        for(int i = 0; i < pathArray.size(); i++){
		        	
		        	pw.println("path:" + pathArray.get(i));
		        }
		        pw.close();
			}
			else{			//ない場合はエラーを返す（作成する、という方法でもいいかもしれない）
				System.out.println("ファイルが見つからないため書き込めません");
			}
			
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	
	
	//データのロードを担当するメソッド。あくまで「データファイル→ArrayList」でのロード
	public void loadData(){
		
		try{
			String str;
			BufferedReader br = new BufferedReader(new FileReader(saveDataFile));
			
			//セーブデータの終わりの行になるまで、という処理
			while((str = br.readLine()) != null){
				if(check == 1){				//一行目は時間の保存領域なので
					
					try{
					str = str.replaceAll("time:", "");
					
					 System.out.println(Arrays.toString(str.toCharArray()));
					tempTime = new Integer(str).intValue();				//今までの累計起動時間を一時保管変数に格納する
					check++;
					}catch(NumberFormatException e){		//ファイルをメモ帳などで開くと「見えない文字」が生成するので消去する
						str = str.substring(1, str.length());
					}
				}else{						//後に色々な条件分岐を入れる
					str = str.replaceAll("path:", "");
					pathArray.add(str);
					
					check++;
				}
				
			}
			
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	
	
	//ファイルのパスを追加するメソッド
	//パスはStringとしてArrayListであるpathArrayに順次追加される
	//ArrayListは配列の進化版で途中の要素を削除してもその部分を詰めたり、
	//要素数を勝手に動的に確保してくれる優れもの
	//今の所万能。ギャルゲーのキャラにいてもいいレベル
	public void addLauncherListSave(String path){
		
		pathArray.add(path);
	}
	
	
	//Launcherで削除したいファイルのパスを受け取ればそのパスをArrayListから削除するメソッド
	public void deleteLauncherListSave(String path){
		
		for(int i = 0; i < pathArray.size(); i++){
			
			if(pathArray.get(i).equals(path)){
				pathArray.remove(i);
			}
		}
	}
	
	
	//Launcher.javaから一時保管用のArrayListをもらってセーブするメソッド
	public void saveLauncherList(ArrayList<String> tempAL){
		
		pathArray = tempAL;
		
	}
	
	
	//呼び出されることでセーブされているLauncher用ファイルデータのArrayListを返す
	public ArrayList<String> loadLauncherList(){
		
		return pathArray;
	}
	
	
	
	//ファイルの有無をチェックするメソッド
	private static boolean checkBeforeWritefile(File file){
	    if (file.exists()){
	      if (file.isFile() && file.canWrite()){
	        return true;
	      }
	    }

	    return false;
	  }
	
}
