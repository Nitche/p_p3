package function;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


//頑張って色々書いたけど、ネットで見つけたコードの方が短かった
//今は色々な機能を沢山つけといて後から最適化する予定

public class CalendarAlpha {

	public Calendar calendar = Calendar.getInstance();
	
	public String[] day_Name = {"日","月","火","水","木","金","土"};		//Calenderクラスの曜日は日曜日から始まる
	//またCalendarクラスのgetメソッドの戻り値（整数、日曜日が1）によって文字を出力するようにするためのもの
	
	//曜日と同じ使用目的。使うかは不明
	public String[] month_Name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	
	public int[] calendarDate;
	public JButton[] calendarButton;
	
	
	
	//各種値の読み込み
	public int year = calendar.get(Calendar.YEAR);
	public int month = calendar.get(Calendar.MONTH) + 1;			//一月がデフォルトで０に設定されている
	public int date = calendar.get(Calendar.DATE);
	public int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;	//日曜日が０になるように調整
	public int am_Pm = calendar.get(Calendar.AM_PM);
	
	//基本的なデータはを格納するint型配列
	public int[] cal_data = {year, month, date, am_Pm};
	
	//年、月、日、時間帯（午前か午後か）をint型配列に格納するコンストラクタ
	//曜日は文字で返したかったので別にした
	//ボツ、いらない
	public void Calendar(){
		
	}
	

	//以下の～Returnメソッドはswitch文を使用したり、かっこが重複することを避けるために設けている
	//本当に正しいかどうかはわからない
	//法律と同じなのよ（遠い目）
	
	//現在の曜日を文字で返すメソッド
	public String dayReturn(){
		
		return day_Name[day];
	}
	
	
	//現在の月を返すメソッド
	public String monthReturn(){
		
		return month_Name[month - 1];
	}
	
	
	//閏年かどうかを判定するメソッド
	//次のmonthMaxReturnで早速使う
	//閏年ならtrueを返す
	//
	//閏年の時はカレンダーに少し装飾をする予定
	public boolean leapYear(){
		
		/*
		if(year % 4 == 0){
			
			if(year % 100 == 0){
				
				if(year )
				
				return false;
			}
			
			return true;
		}
		*/
		
		
		int leapFlag = 0;		//条件分岐を簡略化するためのフラグ。ただし条件分岐前のおぜん立てがとてつもなくめんどい
		
		//以下leapFlagのフラグ判定に使う。
		//「<<」はシフト演算子。二進数において桁を「≪」の右側の値だけ移動させる
		//ex)
		//1<<3 = 1000 = 8 (1 = 0001), 3<<2 = 1100 = 12 (3 = 0011)
		int divide4 = 1 << 2;	//三桁目のフラグが立っているかどうかを判定する	100
		int divide100 = 1 << 1;	//二桁目のフラグが立っているかどうかを判定する	010
		int divide400 = 1 << 0;	//一桁目のフラグが立っているかどうかを判定する	001
		
		
		//以下leapFlagの三桁の初期設定
		leapFlag |= divide4 * ((year % 4 == 0) ? 1:0);		//000  における一番左の桁のフラグ初期化。年が4の倍数なら1
		leapFlag |= divide100 * ((year % 100 == 0) ? 1: 0);	//000  における真ん中の桁のフラグ初期化。年が100の倍数なら1
		leapFlag |= divide400 * ((year % 400 == 0) ? 1: 0);	//000  における一番右の桁のフラグ初期化。年が400の倍数なら1
		
		
		//以下本番
		//上に書いてある訳わかめなif文を少し簡略化
		//我の専門分野、論理回路における「論理式」と「カルノー図」という技術をフルに使った条件分岐だ
		if( ((leapFlag & divide4 & ~divide100) != 0) | ((leapFlag & divide400) != 0) ){
			//順に解説しよう
			//まず年が4で割れる集合をＡ、100で割れる集合をＢ、400で割れる集合をＣとする
			//閏年となる条件をすべて書き出し、それをカルノー図で簡略化する（とても複雑なのでここでは解説しない）
			//それによってできた論理式が  A * (~B) + C  である(  ~  は否定、NOTの意味)
			//また、「＆」は論理積、ANDを意味し二つの2進数の各桁において両方とも1でなければ1が算出されない
			//「｜」は論理和、ORを意味しどちらかが1であれば1が算出される
			//ex) AND : 0 & 1 = 0, 1 & 1 = 1, 010 & 110 = 010
			//ex) OR  : 0 | 1 = 1, 1 | 1 = 1, 0 | 0 = 0, 001 | 100 = 101
			
			return true;		//何重ものif文とelse文はしなくて済むことがこの一行でわかる（説明多いとか言わない)
		}else{
			return false;
		}

	}
	
	
	//その月の最大日数を返すメソッド
	//leapYearメソッドの活用処
	//またleapYearで行った処理の流れを再び使うことになる
	public int monthMaxReturn(int mth){
		
		int monthFlag = 0;		//000 三桁目が偶数月判定、二桁目が７月以下判定、一桁目が二月判定
		
		int monthE = 1 << 2;	//三桁目
		int month7 = 1 << 1;	//二桁目
		int month2 = 1 << 0;	//一桁目
		
		//フラグ初期化
		monthFlag |= monthE * ((mth % 2 == 0) ? 1 : 0);
		monthFlag |= month7 * ((mth <= 7) ? 1 : 0);
		monthFlag |= month2 * ((mth == 2) ? 1 : 0);
		
		
		if( ((monthFlag & monthE) ^ (monthFlag & month7)) == 1){	//排他的論理和 ^ 
			
			return 31;
		}else if( (monthFlag & month2) == 0){
			
			return 30;
		}else{
			
			if(leapYear()){
				return 29;
			}
			
			return 28;
		}
		//これで終わりな訳だが、最後に排他的論理和について説明する
		//排他的論理和とは論理演算の一つで、プログラミングにおいては「＾」を使う
		//二つの事象、集合ＡとＢがあるとして、その二つの排他的論理和はＡＮＤとＯＲを使って、
		// A * (~B) + (~A) + B 
		//を表す
		//ＡＮＤとＯＲの組み合わせでできるため絶対必要なものではないが、
		//今回の条件分岐でも出てきたようにプログラミングでも電子回路でも頻繁に使うため、定義された
		//ＡＮＤとＯＲのようにＸＯＲと名前がついている（EXclusive OR）
		//マインクラフトの赤石回路に触れていたならば聞いたことがあるかもしれない
		
		//ああ、かわいいんじゃ＾～
	}
	
	
	//その月の一日が何曜日かを返すメソッド
	//引数に年と月を入力する
	//ツェラーの公式と呼ばれるもので、詳しくはWikipedia参照
	public int firstDayReturn(int y, int m){
		
		int C = (int) Math.floor(y/100);	//切り捨て
		int Y = y % 100;
		
		int G = (4 <= y && y <= 1582) ? (6 * C + 5) : (5 * C + (int)Math.floor(C /4));
		
		int h;
		
		h = (int)(date + Math.floor((26 * (m+1))/10) + Y + Math.floor(Y/4) + G ) % 7;
		
		return h;	//戻り値は月曜日が１で一ずつ上昇していき、金曜日で６になり、土曜日が０となる土曜日起点の値である
	}
	
	
	
	//日付を配列の中に実際のカレンダー見たく入れるメソッド
	public void dateInsert(){
		
		int firstDate;
		int i;
		
		
		//ツェラーの公式は一月および二月は特例として前年の13月と14月として扱う
		//2014年1月なら2013年13月、と言った具合に
		if(month == 1 || month == 2){
		
			firstDate = firstDayReturn(year - 1, 12 + month);
		}else{
			firstDate = firstDayReturn(year, month);
		}
		
		//一日前の日に０を入れる
		if( (firstDate - 1) != -1){
			for(i = 0; i < firstDate -1; i++){
				calendarDate[i] = 0;
			}
		}else{
			for(i = 0; i < 6; i++){
				calendarDate[i] = 0;
			}
		}
		
		//i番目から順に日付を格納していく
		for(int j = i; j < monthMaxReturn(month) + i; j++){
			calendarDate[j] = (j - i + 1);		// j - i + １ で1から順にインクリメントされる
		}
		
	}
	
	
	//カレンダーを描画するメソッド
	//日にち一個一個をボタンにすることで将来タスク管理プログラムを入れる時に活用できるようにする
	public void paintCalendar(){
		
		JPanel calendarPanel = new JPanel();
		JFrame calendarFrame = new JFrame();
		
		
		calendarPanel.setLayout(null);
		
		for(int i = 0; i < monthMaxReturn(month); i++){
			if(calendarDate[i] != 0){
				
				final int si = i;
				
				calendarButton[i] = new JButton(String.valueOf(calendarDate[i]));
				calendarButton[i].setContentAreaFilled(false);
				calendarButton[i].setForeground(Color.white);
				calendarButton[i].addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						System.out.println(calendarDate[si]);
					}
				});
				
				calendarButton[i].setBounds(50 * (i % 7), 70 * (i / 7), 50, 70);
				
			}
		}
		
		
		
		public void createCalFrame(int w,int h) {
			fl.setBounds(w,h,500,300);
			fl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fl.setUndecorated(true);
			fl.setBackground(new Color(0.2f, 0f, 0f, 0.5f));//赤・緑・青・透明度
			//fl.pack();
			fl.setVisible(true);
			fl.setAlwaysOnTop(true);
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
