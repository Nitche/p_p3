package function;

import java.io.IOException;

import javax.swing.JFrame;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import weather_Enum.PlaceEnum;
import weather_Enum.WeatherEnum;

public class Weather extends JFrame{

	  
	 //private static final String RSS_URL= "http://rss.weather.yahoo.co.jp/rss/days/2400.xml";
	
	
	private static final String rss_URL= "http://rss.weather.yahoo.co.jp/rss/days/";
	private int rss_PlaceNum;					//各市町村の対応する整数
	private PlaceEnum placeEnum;
	private String weatherString = "%";
	private String[] weatherDivString;
	
	
	public void placeCatch(PlaceEnum pe){
		placeEnum = pe;
		
		rss_PlaceNum = placeEnum.YOKOHAMA.getNum();
		
	}
	
	//天気情報を返すメソッド
	public WeatherEnum weatherState(){

		//System.out.println("wwwwww");
		
		if(weatherDivString == null){
			System.out.println("ERROR");
			System.exit(1);
		}
		
		
		if(weatherDivString[1].equals("曇り")){
			return WeatherEnum.CLOUDY;
		}
		else if(weatherDivString[1].equals("快晴")){
			return WeatherEnum.CLEAR_SUNNY;
		}
		else if(weatherDivString[1].equals("雨")){
			return WeatherEnum.RAINY;
		}
		else if(weatherDivString[1].equals("晴れ")){
			return WeatherEnum.SUNNY;
		}
		else if(weatherDivString[1].equals("雪")){
			return WeatherEnum.SNOWY;
		}
		else{
			return WeatherEnum.MISTY;
		}

	}
	
	
	
	 public void weatherScript() {  
	    
	  try {  
	     
	   DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	   DocumentBuilder builder = factory.newDocumentBuilder();
	   Document document = builder.parse(rss_URL + rss_PlaceNum + ".xml");  
	     
	   // ドキュメントのルートを取得  
	   Element root = document.getDocumentElement();  
	     
	  // ルート直下の"channel"に含まれるノードリストを取得  
	  // NodeList channel = root.getElementsByTagName("channel");  
	     
	  // "channel"直下の"title"に含まれるノードリストを取得  
	   //NodeList title = ((Element)channel.item(0)).getElementsByTagName("title");  
	     
	   // とりあえず出力する  
	   //System.out.println("タイトル：" + title.item(0).getFirstChild().getNodeValue());  
	     
	   // 各"item"とその中の"title"と"description"を取得する。       
	   NodeList item_list = root.getElementsByTagName("item");  
	     
	   // item分ループする  
	   for(int i = 0; i < item_list.getLength(); i++) {  //これにより１週間の天気を取得できる
	      
	    Element element = (Element)item_list.item(i);  
	  
	    // title を取得する  
	               // NodeList item_title = element.getElementsByTagName("title");  
	                // description を取得する  
	                NodeList item_description = element.getElementsByTagName("description");  
	  
	                // 出力する  
	                //System.out.println(item_title.item(0).getFirstChild().getNodeValue());  	//地域などの出力
	                //System.out.println(item_description.item(0).getFirstChild().getNodeValue());  //天気と気温の出力
	          
	                weatherString += item_description.item(0).getFirstChild().getNodeValue() + "%";
	                
	   }  

	  weatherString = weatherString.replaceAll(" - ", "%");
	  weatherDivString = weatherString.split("%");


	  
	   
	  } catch (ParserConfigurationException e) {  
	   e.printStackTrace();  
	  } catch (SAXException e) {  
	   e.printStackTrace();  
	  } catch (IOException e) {  
	   e.printStackTrace();  
	  }  
	    
	 }  
	
}



